package com.e16din.simplesocials.managers;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;

import com.e16din.simplesocials.core.BaseSocialActionBarActivity;
import com.e16din.simplesocials.core.interfaces.BaseSocialManger;
import com.e16din.simplesocials.core.interfaces.SuccessListener;
import com.e16din.simplesocials.core.utils.ImageLoader;
import com.e16din.simplesocials.core.utils.ImageLoader.OnImageLoaderListener;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCaptchaDialog;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKSdkListener;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKRequest.VKRequestListener;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiLink;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKAttachments;
import com.vk.sdk.api.model.VKPhotoArray;
import com.vk.sdk.api.model.VKWallPostResult;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;

public class VkontakteManager extends BaseSocialManger {

	private static final String[] VK_SCOPE = new String[] { VKScope.FRIENDS, VKScope.WALL, VKScope.PHOTOS,
			VKScope.NOHTTPS };
	private static SuccessListener authListener = null;

	private VKSdkListener vkListener = new VKSdkListener() {
		@Override
		public void onCaptchaError(VKError captchaError) {
			Log.d("debug_vk", "onCaptchaError: " + authListener);
			if (authListener != null) {
				new VKCaptchaDialog(captchaError).show();
				authListener.onCancel();
				authListener = null;
			}
		}

		@Override
		public void onReceiveNewToken(VKAccessToken newToken) {
			Log.d("debug_vk", "onReceiveNewToken: " + authListener);
			if (authListener != null) {
				setToken(newToken.accessToken);
				authListener.onSuccess(VkontakteManager.this, getToken());
				authListener = null;
			}

		}

		public void onRenewAccessToken(VKAccessToken token) {
			Log.d("debug_vk", "onRenewAccessToken: " + authListener);
			if (authListener != null) {
				setToken(token.accessToken);
				authListener.onSuccess(VkontakteManager.this, getToken());
				authListener = null;
			}
		};

		@Override
		public void onTokenExpired(VKAccessToken expiredToken) {
			Log.d("debug_vk", "onTokenExpired: " + authListener);
			throw new NullPointerException("Vkontakte onTokenExpired");
		}

		@Override
		public void onAccessDenied(VKError authorizationError) {
			Log.d("debug_vk", "onAccessDenied: " + authListener);
			if (authListener != null) {
				if (authorizationError.errorCode == VKError.VK_CANCELED
						|| (authorizationError.errorCode == VKError.VK_API_ERROR && authorizationError.errorReason
								.equals("user_denied"))) {// if cancel
					authListener.onCancel();
					authListener = null;
					return;
				} else
					throw new NullPointerException("Vkontakte onAccessDenied");
			}
		}
	};

	public VkontakteManager(BaseSocialActionBarActivity activity, String appId) {
		super(activity);

		VKUIHelper.onCreate((Activity) activity);
		VKSdk.initialize(vkListener, appId);
	}

	@Override
	public String getName() {
		return "Vkontakte";
	}

	@Override
	public void login(SuccessListener listener) {
		super.login(listener);

		VkontakteManager.authListener = listener;

		if (!isAuth())
			VKSdk.authorize(VK_SCOPE, false, true);
		else if (!VKSdk.isLoggedIn()) {
			VKSdk.wakeUpSession();
			listener.onSuccess(this, null);
			// VKSdk.authorize(VK_SCOPE);
		}
	}

	@Override
	public void logout(SuccessListener listener) {
		super.logout(listener);

		VKSdk.logout();
		listener.onSuccess(this, null);
	}

	@Override
	public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
		Log.d("debug_vk", "onActivityResult vk: "+resultCode);
		if (authListener != null) {
			if (resultCode == Activity.RESULT_CANCELED){
				authListener.onCancel();
				authListener = null;
			}
		}

		VKUIHelper.onActivityResult(activity, requestCode, resultCode, data);
	}

	@Override
	public void onResume(Activity activity) {
		super.onResume(activity);
		VKUIHelper.onResume(activity);
	}

	@Override
	public void onDestroy(Activity activity) {
		super.onDestroy(activity);
		VKUIHelper.onDestroy(activity);
	}

	@Override
	protected void postPhoto(final String message, final String postUrl, final String mediaUrl,
			final SuccessListener listener) {

		// setCurrentListener(listener);
		if (VKSdk.isLoggedIn()) {// share photo
			new ImageLoader(mediaUrl, new OnImageLoaderListener() {
				@Override
				public void onCompleted(byte[] data, Bitmap bitmap) {
					uploadImageVk(message, postUrl, bitmap, listener);
					bitmap.recycle();
				}
			}).execute();
		} else {
			// login and share after
			// loginBeforeSharing(false, message, postUrl, imageUrl, listener);
			authListener = new SuccessListener() {
				@Override
				public void onSuccess(BaseSocialManger manager, String supportStr) {
					postPhoto(message, postUrl, mediaUrl, listener);
				}

				@Override
				public void onFail(String error, BaseSocialManger manager) {
					listener.onFail("loginBeforeSharing fail", manager);
				}

				@Override
				public void onCancel() {
					listener.onCancel();
				}
			};

			login(authListener);
		}
	}

	@Override
	protected void postVideo(final String message, final String postUrl, final String videoUrl,
			final SuccessListener listener) {
		if (VKSdk.isLoggedIn()) {
			// share video
			VKApiLink video = new VKApiLink("http://www.youtube.com/watch?v=" + videoUrl);
			VKRequest post = VKApi.wall().post(
					VKParameters.from(VKApiConst.OWNER_ID, VKSdk.getAccessToken().userId, VKApiConst.ATTACHMENTS,
							new VKAttachments(video), VKApiConst.MESSAGE, message + " " + postUrl));
			post.setModelClass(VKWallPostResult.class);
			post.executeWithListener(new VKRequestListener() {
				private static final long serialVersionUID = 1905359067088369798L;

				@Override
				public void onComplete(VKResponse response) {
					super.onComplete(response);
					listener.onSuccess(VkontakteManager.this, null);
				}

				@Override
				public void onError(VKError error) {
					listener.onFail(error.apiError + " 1 " + error.errorMessage, VkontakteManager.this);
				}
			});
		} else
			// login and share after
			loginBeforeSharing(true, message, postUrl, videoUrl, listener);
	}

	private void uploadImageVk(final String desc, final String postUrl, final Bitmap bitmap,
			final SuccessListener listener) {
		VKRequest request = VKApi.uploadWallPhotoRequest(new VKUploadImage(bitmap, VKImageParameters.jpgImage(0.9f)),
				0, 0);

		request.attempts = 10;
		request.executeWithListener(new VKRequestListener() {
			private static final long serialVersionUID = 154960185797098671L;

			@Override
			public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
				String error = request.toString() + " attemptNumber" + String.valueOf(attemptNumber) + " totalAttempts"
						+ String.valueOf(totalAttempts);
				if (listener != null)
					listener.onFail(error, VkontakteManager.this);
			}

			@Override
			public void onComplete(VKResponse response) {
				bitmap.recycle();
				VKApiPhoto photoModel = ((VKPhotoArray) response.parsedModel).get(0);

				VKRequest post = VKApi.wall().post(
						VKParameters.from(VKApiConst.OWNER_ID, VKSdk.getAccessToken().userId, VKApiConst.ATTACHMENTS,
								new VKAttachments(photoModel), VKApiConst.MESSAGE, desc + " " + postUrl));
				post.setModelClass(VKWallPostResult.class);
				post.executeWithListener(new VKRequestListener() {
					private static final long serialVersionUID = 4854278842406728238L;

					@Override
					public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
						String error = request.toString() + " attemptNumber" + String.valueOf(attemptNumber)
								+ " totalAttempts" + String.valueOf(totalAttempts);
						if (listener != null)
							listener.onFail(error, VkontakteManager.this);
						Log.d("debug_vk", "attemptFailed: "+listener);
					}

					@Override
					public void onComplete(VKResponse response) {
						super.onComplete(response);
						if (listener != null)
							listener.onSuccess(VkontakteManager.this, postUrl.substring(postUrl.lastIndexOf("/") + 1));
						Log.d("debug_vk", "onComplete: "+listener);
					}

					@Override
					public void onError(VKError error) {
						if (listener != null)
							listener.onFail(error.apiError + " 2 " + error.errorMessage, VkontakteManager.this);
						Log.d("debug_vk", "onError: "+error.apiError + " 2 " + error.errorMessage);
					}
				});
			}

			@Override
			public void onError(VKError error) {
				if (listener != null)
					listener.onFail(error.apiError + " 3 " + error.errorMessage, VkontakteManager.this);
			}
		});
	}
}
